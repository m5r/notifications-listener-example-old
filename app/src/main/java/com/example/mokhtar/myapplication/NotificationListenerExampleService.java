package com.example.mokhtar.myapplication;

import android.content.Intent;
import android.os.IBinder;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;

import static android.app.Notification.EXTRA_TEXT;
import static android.app.Notification.EXTRA_TITLE;

public class NotificationListenerExampleService extends NotificationListenerService {
    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        Intent intent = new Intent("com.example.mokhtar.myapplication");
        intent.putExtra("origin", sbn.getPackageName());
        intent.putExtra("content", sbn.getNotification().extras.getString(EXTRA_TITLE));
        sendBroadcast(intent);
    }
}
